asgiref==3.2.10
coverage==5.3
dj-database-url==0.5.0
Django==3.1.1
gunicorn @ git+https://github.com/benoitc/gunicorn.git@548d5828da6b93fa6a14217742c6e6d2c7b2b900
psycopg2-binary==2.8.6
pytz==2020.1
selenium==3.141.0
sqlparse==0.3.1
urllib3==1.25.10
whitenoise==5.2.0

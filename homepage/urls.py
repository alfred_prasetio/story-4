from django.urls import path
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.profile, name='profile'),
    path('Hobi/', views.hobi, name='hobi'),
    path('Pengalaman/', views.pengalaman, name='pengalaman'),
    path('Contact/', views.contact, name='contact'),
]
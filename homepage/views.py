from django.shortcuts import render

# Create your views here.
def profile(request):
    return render(request, 'Profile.html')

def pengalaman(request):
    return render(request, 'Pengalaman.html')

def hobi(request):
    return render(request, 'Hobi.html')

def contact(request):
    return render(request, 'Contact.html')